package com.bekiratas.ecommerce.api;

import com.bekiratas.ecommerce.api.base.BaseApi;
import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.request.RequestCategoryAdd;
import com.bekiratas.ecommerce.request.RequestCategoryUpdate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoryApi extends BaseApi {

    CategoryDTO get(Integer id);

    Page<CategoryDTO> getAll(Pageable pageable);

    Integer add(RequestCategoryAdd category);

    void update(RequestCategoryUpdate category);

    void delete(Integer id);
}
