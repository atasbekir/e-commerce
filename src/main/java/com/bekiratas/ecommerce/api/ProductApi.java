package com.bekiratas.ecommerce.api;

import com.bekiratas.ecommerce.api.base.BaseApi;
import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.request.RequestProductAdd;
import com.bekiratas.ecommerce.request.RequestProductUpdate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductApi extends BaseApi {

    ProductDTO get(Integer id);

    Page<ProductDTO> getAll(Pageable pageable);

    Page<ProductDTO> getAllByCategoryId(Integer categoryId, Pageable pageable);

    Integer add(RequestProductAdd product);

    void update(RequestProductUpdate product);

    void delete(Integer id);
}
