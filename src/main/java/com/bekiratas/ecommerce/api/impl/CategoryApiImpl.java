package com.bekiratas.ecommerce.api.impl;

import com.bekiratas.ecommerce.api.CategoryApi;
import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.request.RequestCategoryAdd;
import com.bekiratas.ecommerce.request.RequestCategoryUpdate;
import com.bekiratas.ecommerce.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

@RequestMapping("/api/categories")
@RequestScope
@RestController
public class CategoryApiImpl implements CategoryApi {

    private final CategoryService service;

    @Autowired
    public CategoryApiImpl(CategoryService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    @Override
    public CategoryDTO get(@PathVariable Integer id) {
        return service.get(id);
    }

    @GetMapping
    @Override
    public Page<CategoryDTO> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    @Override
    public Integer add(@Valid @RequestBody RequestCategoryAdd category) {
        return service.add(category);
    }

    @PutMapping
    @Override
    public void update(@Valid @RequestBody RequestCategoryUpdate category) {
        service.update(category);
    }

    @DeleteMapping("/{id}")
    @Override
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }
}
