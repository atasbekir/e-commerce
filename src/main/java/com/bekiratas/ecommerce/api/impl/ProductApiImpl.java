package com.bekiratas.ecommerce.api.impl;

import com.bekiratas.ecommerce.api.ProductApi;
import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.request.RequestProductAdd;
import com.bekiratas.ecommerce.request.RequestProductUpdate;
import com.bekiratas.ecommerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RequestMapping("/api/products")
@RestController
public class ProductApiImpl implements ProductApi {

    private final ProductService service;

    @Autowired
    public ProductApiImpl(ProductService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    @Override
    public ProductDTO get(@PathVariable Integer id) {
        return service.get(id);
    }

    @GetMapping
    @Override
    public Page<ProductDTO> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @GetMapping("/category/{categoryId}")
    @Override
    public Page<ProductDTO> getAllByCategoryId(@PathVariable Integer categoryId, Pageable pageable) {
        return service.getAllByCategoryId(categoryId, pageable);
    }

    @PostMapping
    @Override
    public Integer add(@RequestBody @Valid RequestProductAdd product) {
        return service.add(product);
    }

    @PutMapping
    @Override
    public void update(@RequestBody @Valid RequestProductUpdate product) {
        service.update(product);
    }

    @DeleteMapping("/{id}")
    @Override
    public void delete(@Valid @NotNull @PathVariable Integer id) {
        service.delete(id);
    }
}
