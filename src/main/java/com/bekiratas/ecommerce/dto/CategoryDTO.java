package com.bekiratas.ecommerce.dto;

import com.bekiratas.ecommerce.dto.base.BaseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryDTO extends BaseDTO<Integer> {

    public CategoryDTO(Integer id) {
        super(id);
    }

    @NotNull
    private String name;
}
