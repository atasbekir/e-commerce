package com.bekiratas.ecommerce.dto;

import com.bekiratas.ecommerce.dto.base.BaseDTO;
import com.bekiratas.ecommerce.enums.UnityType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductDTO extends BaseDTO<Integer> {

    public ProductDTO(Integer id) {
        super(id);
    }

    @NotNull
    private Integer categoryId;
    @NotNull
    private String name;
    @NotNull
    private UnityType unitType;
    @NotNull
    private BigDecimal price;
}
