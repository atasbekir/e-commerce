package com.bekiratas.ecommerce.dto.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BaseDTO<ID extends Serializable> implements Serializable {

    private ID id;
}
