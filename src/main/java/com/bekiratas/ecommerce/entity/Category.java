package com.bekiratas.ecommerce.entity;

import com.bekiratas.ecommerce.entity.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "CATEGORY", schema = "E_COMMERCE")
@Entity
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class Category extends BaseEntity<Integer> {

    public Category(Integer id) {
        super(id);
    }

    @Column(name = "NAME")
    private String name;
}
