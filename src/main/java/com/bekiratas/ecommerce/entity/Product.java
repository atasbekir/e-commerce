package com.bekiratas.ecommerce.entity;

import com.bekiratas.ecommerce.entity.base.BaseEntity;
import com.bekiratas.ecommerce.enums.UnityType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "PRODUCT", schema = "E_COMMERCE")
@Entity
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class Product extends BaseEntity<Integer> {

    public Product(Integer id) {
        super(id);
    }

    @JoinColumn(name = "CATEGORY_ID")
    @ManyToOne
    private Category category;
    @Column(name = "NAME")
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(name = "UNIT_TYPE")
    private UnityType unitType;
    @Column(name = "PRICE", precision = 18, scale = 2)
    private BigDecimal price;
}
