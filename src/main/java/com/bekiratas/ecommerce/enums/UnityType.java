package com.bekiratas.ecommerce.enums;

public enum UnityType {
    PIECE,
    GRAM,
    METER
}
