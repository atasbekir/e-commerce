package com.bekiratas.ecommerce.mapper;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.entity.Category;
import com.bekiratas.ecommerce.mapper.base.BaseMapper;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper implements BaseMapper<Category, CategoryDTO> {

    @Override
    public Category toEntity(CategoryDTO dto) {
        if (dto == null) {
            return null;
        }
        Category entity = new Category();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }

    @Override
    public CategoryDTO toDTO(Category entity) {
        if (entity == null) {
            return null;
        }
        CategoryDTO dto = new CategoryDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }
}
