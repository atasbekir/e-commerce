package com.bekiratas.ecommerce.mapper;

import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.entity.Category;
import com.bekiratas.ecommerce.entity.Product;
import com.bekiratas.ecommerce.mapper.base.BaseMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper implements BaseMapper<Product, ProductDTO> {

    @Override
    public Product toEntity(ProductDTO dto) {
        if (dto == null) {
            return null;
        }
        Product entity = new Product();
        entity.setId(dto.getId());
        if (dto.getCategoryId() != null) {
            entity.setCategory(new Category(dto.getCategoryId()));
        }
        entity.setName(dto.getName());
        entity.setUnitType(dto.getUnitType());
        entity.setPrice(dto.getPrice());
        return entity;
    }

    @Override
    public ProductDTO toDTO(Product entity) {
        if (entity == null) {
            return null;
        }
        ProductDTO dto = new ProductDTO();
        dto.setId(entity.getId());
        if (entity.getCategory() != null) {
            dto.setCategoryId(entity.getCategory().getId());
        }
        dto.setName(entity.getName());
        dto.setUnitType(entity.getUnitType());
        dto.setPrice(entity.getPrice());
        return dto;
    }
}
