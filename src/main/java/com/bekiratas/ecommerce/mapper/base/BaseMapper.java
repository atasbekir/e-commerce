package com.bekiratas.ecommerce.mapper.base;

public interface BaseMapper<Entity, DTO> {

    Entity toEntity(DTO dto);

    DTO toDTO(Entity entity);
}
