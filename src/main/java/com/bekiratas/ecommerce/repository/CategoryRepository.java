package com.bekiratas.ecommerce.repository;

import com.bekiratas.ecommerce.entity.Category;
import com.bekiratas.ecommerce.repository.base.BaseRepository;

public interface CategoryRepository extends BaseRepository<Category, Integer> {

}
