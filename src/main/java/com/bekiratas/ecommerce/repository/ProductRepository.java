package com.bekiratas.ecommerce.repository;

import com.bekiratas.ecommerce.entity.Product;
import com.bekiratas.ecommerce.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductRepository extends BaseRepository<Product, Integer> {

    Page<Product> findAllByCategoryId(Integer categoryId, Pageable pageable);
}
