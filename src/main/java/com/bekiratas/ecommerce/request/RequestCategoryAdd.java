package com.bekiratas.ecommerce.request;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Null;

@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class RequestCategoryAdd extends CategoryDTO {

    @Null
    private Integer id;
}
