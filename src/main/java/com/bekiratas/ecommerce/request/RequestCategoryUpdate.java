package com.bekiratas.ecommerce.request;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class RequestCategoryUpdate extends CategoryDTO {

    @NotNull
    private Integer id;
}
