package com.bekiratas.ecommerce.request;

import com.bekiratas.ecommerce.dto.ProductDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class RequestProductUpdate extends ProductDTO {

    @NotNull
    private Integer id;
}
