package com.bekiratas.ecommerce.service;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.service.base.BaseCrudService;

public interface CategoryService extends BaseCrudService<CategoryDTO, Integer> {

}
