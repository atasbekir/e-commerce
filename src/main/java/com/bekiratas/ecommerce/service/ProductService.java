package com.bekiratas.ecommerce.service;

import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.service.base.BaseCrudService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService extends BaseCrudService<ProductDTO, Integer> {

    Page<ProductDTO> getAllByCategoryId(Integer categoryId, Pageable pageable);
}
