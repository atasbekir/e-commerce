package com.bekiratas.ecommerce.service.base;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BaseCrudService<T, ID> {

    T get(ID id);

    Page<T> getAll(Pageable pageable);

    ID add(T dto);

    void update(T dto);

    void delete(ID id);
}
