package com.bekiratas.ecommerce.service.impl;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.mapper.CategoryMapper;
import com.bekiratas.ecommerce.repository.CategoryRepository;
import com.bekiratas.ecommerce.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;
    private final CategoryMapper mapper;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository, CategoryMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public CategoryDTO get(Integer id) {
        return mapper.toDTO(repository.getOne(id));
    }

    @Override
    public Page<CategoryDTO> getAll(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::toDTO);
    }

    @Override
    public Integer add(CategoryDTO category) {
        return repository.saveAndFlush(mapper.toEntity(category)).getId();
    }

    @Override
    public void update(CategoryDTO category) {
        if (!repository.existsById(category.getId())) {
            throw new RuntimeException("The category number " + category.getId() + " was not found");
        }
        repository.saveAndFlush(mapper.toEntity(category));
    }

    @Override
    public void delete(Integer id) {
        if (!repository.existsById(id)) {
            throw new RuntimeException("The category number " + id + " was not found");
        }
        repository.deleteById(id);
        repository.flush();
    }
}
