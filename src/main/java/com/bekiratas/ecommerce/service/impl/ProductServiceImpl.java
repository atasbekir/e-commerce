package com.bekiratas.ecommerce.service.impl;

import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.mapper.ProductMapper;
import com.bekiratas.ecommerce.repository.ProductRepository;
import com.bekiratas.ecommerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Autowired
    public ProductServiceImpl(ProductRepository repository, ProductMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public ProductDTO get(Integer id) {
        return mapper.toDTO(repository.getOne(id));
    }

    @Override
    public Page<ProductDTO> getAll(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::toDTO);
    }

    @Override
    public Page<ProductDTO> getAllByCategoryId(Integer categoryId, Pageable pageable) {
        return repository.findAllByCategoryId(categoryId, pageable).map(mapper::toDTO);
    }

    @Override
    public Integer add(ProductDTO product) {
        return repository.saveAndFlush(mapper.toEntity(product)).getId();
    }

    @Override
    public void update(ProductDTO product) {
        if (!repository.existsById(product.getId())) {
            throw new RuntimeException("The product number " + product.getId() + " was not found");
        }
        repository.saveAndFlush(mapper.toEntity(product));
    }

    @Override
    public void delete(Integer id) {
        if (!repository.existsById(id)) {
            throw new RuntimeException("The product number " + id + " was not found");
        }
        repository.deleteById(id);
        repository.flush();
    }
}
