package com.bekiratas.ecommerce.api;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.request.RequestCategoryAdd;
import com.bekiratas.ecommerce.request.RequestCategoryUpdate;
import com.bekiratas.ecommerce.service.CategoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CategoryApi.class)
@RunWith(SpringRunner.class)
public class CategoryApiImplTest {

    @MockBean
    private CategoryService service;
    @Autowired
    private MockMvc mockMvc;
    private CategoryDTO category;
    @Autowired
    private ObjectMapper mapper;
    @Before
    public void setUp() {
        category = new CategoryDTO(1);
        category.setName("name");
    }

    @Test
    public void testGet() throws Exception {
        when(service.get(any())).thenReturn(category);
        mockMvc.perform(get("/api/categories/{id}", category.getId()).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(category.getId()))
                .andExpect(jsonPath("$.name").value(category.getName()));
    }

    @Test
    public void testGetAll() throws Exception {
        PageRequest pageable = PageRequest.of(0, 5);
        Page<CategoryDTO> categoryPage = new PageImpl<>(Collections.singletonList(category), pageable, 1);
        when(service.getAll(any())).thenReturn(categoryPage);
        String sort = pageable.getSort().toString();
        String size = String.valueOf(pageable.getPageSize());
        String page = String.valueOf(pageable.getPageNumber());
        mockMvc.perform(get("/api/categories")
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("page", page)
                .queryParam("size", size)
                .queryParam("sort", sort))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.pageNumber").value(categoryPage.getPageable().getPageNumber()))
                .andExpect(jsonPath("$.pageable.pageSize").value(categoryPage.getPageable().getPageSize()))
                .andExpect(jsonPath("$.totalElements").value(categoryPage.getTotalElements()))
                .andExpect(jsonPath("$.totalPages").value(categoryPage.getTotalPages()))
                .andExpect(jsonPath("$.content.length()").value(categoryPage.getContent().size()))
                .andExpect(jsonPath("$.content[0].id").value(categoryPage.getContent().get(0).getId()))
                .andExpect(jsonPath("$.content[0].name").value(categoryPage.getContent().get(0).getName()));
    }

    @Test
    public void testAdd() throws Exception {
        RequestCategoryAdd categoryAdd=new RequestCategoryAdd();
        categoryAdd.setName("name");
        Integer id = 1;
        when(service.add(any())).thenReturn(id);
        mockMvc.perform(post("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(categoryAdd)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(id));
    }

    @Test
    public void testUpdate() throws Exception {
        RequestCategoryUpdate categoryUpdate=new RequestCategoryUpdate();
        categoryUpdate.setId(1);
        categoryUpdate.setName("name");
        doNothing().when(service).update(any());
        mockMvc.perform(put("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(categoryUpdate)))
                .andDo(print())
                .andExpect(status().isOk());
        verify(service, times(1)).update(any());
    }

    @Test
    public void testDelete() throws Exception {
        doNothing().when(service).delete(any());
        mockMvc.perform(delete("/api/categories/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        verify(service, times(1)).delete(any());
    }
}
