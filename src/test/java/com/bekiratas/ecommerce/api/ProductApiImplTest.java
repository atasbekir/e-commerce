package com.bekiratas.ecommerce.api;

import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.enums.UnityType;
import com.bekiratas.ecommerce.request.RequestProductAdd;
import com.bekiratas.ecommerce.request.RequestProductUpdate;
import com.bekiratas.ecommerce.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductApi.class)
@RunWith(SpringRunner.class)
public class ProductApiImplTest {

    @MockBean
    private ProductService service;
    @Autowired
    private MockMvc mockMvc;
    private ProductDTO product;
    @Autowired
    private ObjectMapper mapper;

    @Before
    public void setUp() {
        product = new ProductDTO(1);
        product.setName("name");
    }

    @Test
    public void testGet() throws Exception {
        when(service.get(any())).thenReturn(product);
        mockMvc.perform(get("/api/products/{id}", product.getId()).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(product.getId()))
                .andExpect(jsonPath("$.categoryId").value(product.getCategoryId()))
                .andExpect(jsonPath("$.name").value(product.getName()))
                .andExpect(jsonPath("$.unitType").value(product.getUnitType()))
                .andExpect(jsonPath("$.price").value(product.getPrice()));
    }

    @Test
    public void testGetAll() throws Exception {
        PageRequest pageable = PageRequest.of(0, 5);
        Page<ProductDTO> productPage = new PageImpl<>(Collections.singletonList(product), pageable, 1);
        when(service.getAll(any())).thenReturn(productPage);
        String sort = pageable.getSort().toString();
        String size = String.valueOf(pageable.getPageSize());
        String page = String.valueOf(pageable.getPageNumber());
        mockMvc.perform(get("/api/products")
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("page", page)
                .queryParam("size", size)
                .queryParam("sort", sort))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.pageNumber").value(productPage.getPageable().getPageNumber()))
                .andExpect(jsonPath("$.pageable.pageSize").value(productPage.getPageable().getPageSize()))
                .andExpect(jsonPath("$.totalElements").value(productPage.getTotalElements()))
                .andExpect(jsonPath("$.totalPages").value(productPage.getTotalPages()))
                .andExpect(jsonPath("$.content.length()").value(productPage.getContent().size()))
                .andExpect(jsonPath("$.content[0].id").value(productPage.getContent().get(0).getId()))
                .andExpect(jsonPath("$.content[0].categoryId").value(productPage.getContent().get(0).getCategoryId()))
                .andExpect(jsonPath("$.content[0].name").value(productPage.getContent().get(0).getName()))
                .andExpect(jsonPath("$.content[0].unitType").value(productPage.getContent().get(0).getUnitType()))
                .andExpect(jsonPath("$.content[0].price").value(productPage.getContent().get(0).getPrice()));
    }

    @Test
    public void testGetAllByCategoryId() throws Exception {
        PageRequest pageable = PageRequest.of(0, 5);
        Page<ProductDTO> productPage = new PageImpl<>(Collections.singletonList(product), pageable, 1);
        when(service.getAllByCategoryId(any(), any())).thenReturn(productPage);
        String sort = pageable.getSort().toString();
        String size = String.valueOf(pageable.getPageSize());
        String page = String.valueOf(pageable.getPageNumber());
        mockMvc.perform(get("/api/products/category/{categoryId}", 1)
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("page", page)
                .queryParam("size", size)
                .queryParam("sort", sort))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageable.pageNumber").value(productPage.getPageable().getPageNumber()))
                .andExpect(jsonPath("$.pageable.pageSize").value(productPage.getPageable().getPageSize()))
                .andExpect(jsonPath("$.totalElements").value(productPage.getTotalElements()))
                .andExpect(jsonPath("$.totalPages").value(productPage.getTotalPages()))
                .andExpect(jsonPath("$.content.length()").value(productPage.getContent().size()))
                .andExpect(jsonPath("$.content[0].id").value(productPage.getContent().get(0).getId()))
                .andExpect(jsonPath("$.content[0].categoryId").value(productPage.getContent().get(0).getCategoryId()))
                .andExpect(jsonPath("$.content[0].name").value(productPage.getContent().get(0).getName()))
                .andExpect(jsonPath("$.content[0].unitType").value(productPage.getContent().get(0).getUnitType()))
                .andExpect(jsonPath("$.content[0].price").value(productPage.getContent().get(0).getPrice()));
    }

    @Test
    public void testAdd() throws Exception {
        RequestProductAdd productAdd = new RequestProductAdd();
        productAdd.setCategoryId(1);
        productAdd.setName("name");
        productAdd.setUnitType(UnityType.PIECE);
        productAdd.setPrice(new BigDecimal("10.25"));
        Integer id = 1;
        when(service.add(any())).thenReturn(id);
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(productAdd)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(id));
    }

    @Test
    public void testUpdate() throws Exception {
        RequestProductUpdate productUpdate = new RequestProductUpdate();
        productUpdate.setId(1);
        productUpdate.setCategoryId(1);
        productUpdate.setName("name");
        productUpdate.setUnitType(UnityType.PIECE);
        productUpdate.setPrice(new BigDecimal("10.25"));
        doNothing().when(service).update(any());
        mockMvc.perform(put("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(productUpdate)))
                .andDo(print())
                .andExpect(status().isOk());
        verify(service, times(1)).update(any());
    }

    @Test
    public void testDelete() throws Exception {
        doNothing().when(service).delete(any());
        mockMvc.perform(delete("/api/products/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        verify(service, times(1)).delete(any());
    }
}
