package com.bekiratas.ecommerce.mapper;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.entity.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryMapperTest {

    @Autowired
    private CategoryMapper mapper;

    @Test
    public void testToEntity() {
        CategoryDTO dto = new CategoryDTO();
        dto.setId(1);
        dto.setName("name");
        Category entity = mapper.toEntity(dto);
        assertThat(entity.getId()).isEqualByComparingTo(dto.getId());
        assertThat(entity.getName()).isEqualTo(dto.getName());
    }

    @Test
    public void testToDTO() {
        Category entity = new Category();
        entity.setId(1);
        entity.setName("name");
        CategoryDTO dto = mapper.toDTO(entity);
        assertThat(dto.getId()).isEqualByComparingTo(entity.getId());
        assertThat(dto.getName()).isEqualTo(entity.getName());
    }
}
