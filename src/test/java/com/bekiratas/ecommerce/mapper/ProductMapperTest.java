package com.bekiratas.ecommerce.mapper;

import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.entity.Category;
import com.bekiratas.ecommerce.entity.Product;
import com.bekiratas.ecommerce.enums.UnityType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductMapperTest {

    @Autowired
    private ProductMapper mapper;

    @Test
    public void testToEntity_categoryIdNotNull() {
        ProductDTO dto = new ProductDTO();
        dto.setId(1);
        dto.setCategoryId(1);
        dto.setName("name");
        dto.setUnitType(UnityType.PIECE);
        dto.setPrice(new BigDecimal("10.25"));
        Product entity = mapper.toEntity(dto);
        assertThat(entity.getId()).isEqualByComparingTo(dto.getId());
        assertThat(entity.getCategory()).isNotNull();
        assertThat(entity.getCategory().getId()).isEqualByComparingTo(dto.getId());
        assertThat(entity.getName()).isEqualTo(dto.getName());
        assertThat(entity.getUnitType()).isEqualTo(dto.getUnitType());
        assertThat(entity.getPrice()).isEqualByComparingTo(dto.getPrice());
    }

    @Test
    public void testToEntity_categoryIdNull() {
        ProductDTO dto = new ProductDTO();
        dto.setId(1);
        dto.setName("name");
        dto.setUnitType(UnityType.PIECE);
        dto.setPrice(new BigDecimal("10.25"));
        Product entity = mapper.toEntity(dto);
        assertThat(entity.getId()).isEqualByComparingTo(dto.getId());
        assertThat(entity.getCategory()).isNull();
        assertThat(entity.getName()).isEqualTo(dto.getName());
        assertThat(entity.getUnitType()).isEqualTo(dto.getUnitType());
        assertThat(entity.getPrice()).isEqualByComparingTo(dto.getPrice());
    }

    @Test
    public void testToDTO_categoryNotNull() {
        Product entity = new Product();
        entity.setId(1);
        entity.setCategory(new Category(1));
        entity.setName("name");
        entity.setUnitType(UnityType.PIECE);
        entity.setPrice(new BigDecimal("10.25"));
        ProductDTO dto = mapper.toDTO(entity);
        assertThat(dto.getId()).isEqualByComparingTo(entity.getId());
        assertThat(entity.getCategory()).isNotNull();
        assertThat(dto.getCategoryId()).isEqualByComparingTo(entity.getCategory().getId());
        assertThat(dto.getName()).isEqualTo(entity.getName());
        assertThat(dto.getUnitType()).isEqualTo(entity.getUnitType());
        assertThat(dto.getPrice()).isEqualByComparingTo(entity.getPrice());
    }

    @Test
    public void testToDTO_categoryNull() {
        Product entity = new Product();
        entity.setId(1);
        entity.setName("name");
        entity.setUnitType(UnityType.PIECE);
        entity.setPrice(new BigDecimal("10.25"));
        ProductDTO dto = mapper.toDTO(entity);
        assertThat(dto.getId()).isEqualByComparingTo(entity.getId());
        assertThat(dto.getCategoryId()).isNull();
        assertThat(dto.getName()).isEqualTo(entity.getName());
        assertThat(dto.getUnitType()).isEqualTo(entity.getUnitType());
        assertThat(dto.getPrice()).isEqualByComparingTo(entity.getPrice());
    }
}
