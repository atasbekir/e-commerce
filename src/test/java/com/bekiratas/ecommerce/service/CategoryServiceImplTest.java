package com.bekiratas.ecommerce.service;

import com.bekiratas.ecommerce.dto.CategoryDTO;
import com.bekiratas.ecommerce.entity.Category;
import com.bekiratas.ecommerce.mapper.CategoryMapper;
import com.bekiratas.ecommerce.repository.CategoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoryServiceImplTest {

    @MockBean
    private CategoryRepository repository;
    @MockBean
    private CategoryMapper mapper;
    @Autowired
    private CategoryService service;
    private CategoryDTO categoryDTO;
    private Category category;

    @Before
    public void setUp() {
        categoryDTO = new CategoryDTO(1);
        categoryDTO.setName("name");
    }

    @Test
    public void testGet() {
        when(repository.getOne(anyInt())).thenReturn(category);
        when(mapper.toDTO(category)).thenReturn(categoryDTO);
        CategoryDTO dto = service.get(anyInt());
        assertThat(categoryDTO.getId()).isEqualByComparingTo(dto.getId());
        assertThat(categoryDTO.getName()).isEqualTo(dto.getName());
    }

    @Test
    public void testGetAll() {
        PageRequest pageable = PageRequest.of(0, 5);
        long totalElements = 1L;
        Page<Category> page = new PageImpl<>(Collections.singletonList(category), pageable, totalElements);
        when(repository.findAll(pageable)).thenReturn(page);
        when(mapper.toDTO(category)).thenReturn(categoryDTO);
        Page<CategoryDTO> categoryPage = service.getAll(pageable);
        assertThat(totalElements).isEqualTo(categoryPage.getTotalElements());
        assertThat(pageable).isEqualTo(categoryPage.getPageable());
    }

    @Test
    public void testAdd() {
        Category savedCategory = new Category(2);
        when(mapper.toEntity(categoryDTO)).thenReturn(category);
        when(repository.saveAndFlush(category)).thenReturn(savedCategory);
        Integer id = service.add(categoryDTO);
        assertThat(savedCategory.getId()).isEqualByComparingTo(id);
    }

    @Test
    public void testUpdate_existsById_returns_true() {
        when(repository.existsById(anyInt())).thenReturn(true);
        when(mapper.toEntity(categoryDTO)).thenReturn(category);
        when(repository.saveAndFlush(category)).thenReturn(category);
        service.update(categoryDTO);
        verify(repository, times(1)).existsById(anyInt());
        verify(repository, times(1)).saveAndFlush(category);
    }

    @Test(expected = Exception.class)
    public void testUpdate_existsById_returns_false() {
        when(repository.existsById(anyInt())).thenReturn(false);
        service.update(categoryDTO);
    }

    @Test
    public void testDelete_existsById_returns_true() {
        when(repository.existsById(anyInt())).thenReturn(true);
        doNothing().when(repository).deleteById(anyInt());
        service.delete(anyInt());
        verify(repository, times(1)).existsById(anyInt());
        verify(repository, times(1)).deleteById(anyInt());
    }

    @Test(expected = Exception.class)
    public void testDelete_existsById_returns_false() {
        when(repository.existsById(anyInt())).thenReturn(false);
        service.delete(anyInt());
    }
}
