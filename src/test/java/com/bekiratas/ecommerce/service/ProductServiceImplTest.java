package com.bekiratas.ecommerce.service;

import com.bekiratas.ecommerce.dto.ProductDTO;
import com.bekiratas.ecommerce.entity.Product;
import com.bekiratas.ecommerce.enums.UnityType;
import com.bekiratas.ecommerce.mapper.ProductMapper;
import com.bekiratas.ecommerce.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Collections;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductServiceImplTest {

    @MockBean
    private ProductRepository repository;
    @MockBean
    private ProductMapper mapper;
    @Autowired
    private ProductService service;
    private ProductDTO productDTO;
    private Product product;

    @Before
    public void setUp() {
        productDTO = new ProductDTO(1);
        productDTO.setCategoryId(1);
        productDTO.setName("name");
        productDTO.setUnitType(UnityType.PIECE);
        productDTO.setPrice(BigDecimal.ONE);
    }

    @Test
    public void testGet() {
        when(repository.getOne(anyInt())).thenReturn(product);
        when(mapper.toDTO(product)).thenReturn(productDTO);
        ProductDTO dto = service.get(anyInt());
        assertThat(productDTO.getId()).isEqualByComparingTo(dto.getId());
        assertThat(productDTO.getCategoryId()).isEqualByComparingTo(dto.getCategoryId());
        assertThat(productDTO.getName()).isEqualTo(dto.getName());
        assertThat(productDTO.getUnitType()).isEqualTo(dto.getUnitType());
        assertThat(productDTO.getPrice()).isEqualByComparingTo(dto.getPrice());
    }

    @Test
    public void testGetAll() {
        PageRequest pageable = PageRequest.of(0, 5);
        long totalElements = 1L;
        Page<Product> page = new PageImpl<>(Collections.singletonList(product), pageable, totalElements);
        when(repository.findAll(pageable)).thenReturn(page);
        when(mapper.toDTO(product)).thenReturn(productDTO);
        Page<ProductDTO> productPage = service.getAll(pageable);
        assertThat(totalElements).isEqualTo(productPage.getTotalElements());
        assertThat(pageable).isEqualTo(productPage.getPageable());
    }

    @Test
    public void testGetAllByCategoryId() {
        PageRequest pageable = PageRequest.of(0, 5);
        long totalElements = 1L;
        int categoryId = 1;
        Page<Product> page = new PageImpl<>(Collections.singletonList(product), pageable, totalElements);
        when(repository.findAllByCategoryId(categoryId, pageable)).thenReturn(page);
        when(mapper.toDTO(product)).thenReturn(productDTO);
        Page<ProductDTO> productPage = service.getAllByCategoryId(categoryId, pageable);
        assertThat(totalElements).isEqualTo(productPage.getTotalElements());
        assertThat(pageable).isEqualTo(productPage.getPageable());
    }

    @Test
    public void testAdd() {
        Product savedProduct = new Product(2);
        when(mapper.toEntity(productDTO)).thenReturn(product);
        when(repository.saveAndFlush(product)).thenReturn(savedProduct);
        Integer id = service.add(productDTO);
        assertThat(savedProduct.getId()).isEqualByComparingTo(id);
    }

    @Test
    public void testUpdate_existsById_returns_true() {
        when(repository.existsById(anyInt())).thenReturn(true);
        when(mapper.toEntity(productDTO)).thenReturn(product);
        when(repository.saveAndFlush(product)).thenReturn(product);
        service.update(productDTO);
        verify(repository, times(1)).existsById(anyInt());
        verify(repository, times(1)).saveAndFlush(product);
    }

    @Test(expected = Exception.class)
    public void testUpdate_existsById_returns_false() {
        when(repository.existsById(anyInt())).thenReturn(false);
        service.update(productDTO);
    }

    @Test
    public void testDelete_existsById_returns_true() {
        when(repository.existsById(anyInt())).thenReturn(true);
        doNothing().when(repository).deleteById(anyInt());
        service.delete(anyInt());
        verify(repository, times(1)).existsById(anyInt());
        verify(repository, times(1)).deleteById(anyInt());
    }

    @Test(expected = Exception.class)
    public void testDelete_existsById_returns_false() {
        when(repository.existsById(anyInt())).thenReturn(false);
        service.delete(anyInt());
    }
}
